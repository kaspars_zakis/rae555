dtmc

const double 	p;
const int 	N;
const double q=1-p;

module lietus_X
	s : [0..N] init N;

	[] s=0 		->	(s'=N);
	[] s>0 & s<N	->	p:(s'= s=1? N:s-1)	+	q:(s'= s=N-1 ? s:s+1);
	[] s=N		->	p:(s'=1)		+	q:(s'=0);
endmodule
 
rewards "saliit"
	s=0 : 1*p;
endrewards