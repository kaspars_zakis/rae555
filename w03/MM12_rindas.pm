dtmc

const int L = 8;
const int M = 9;
const int sakums;

const double pL = L/(L+M);
const double pM = M/(L+M);

module rindas
	s : [0..2] init sakums;

	[] s=0|s=2-> (s'=1);
	[] s=1 -> pM : (s'=0) + pL : (s'=2); 
endmodule