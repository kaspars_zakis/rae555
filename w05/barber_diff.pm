// Continuous-time Markov chain
ctmc
// rindas izmērs
const int q_max = 4;
// klientu biežums
const double rate_arrive = 5;
module SQ

// klientu skaits
	q : [0..q_max] init 0;
// zaudētie klienti, kad rinda ir pilna
//	z : [0..1000];
// klientu pien�?kšana
	[atnaak] true -> rate_arrive : (q'=min(q+1,q_max));//&(z'=q=q_max?z+1:z);
// klientu apkalpošana
	[serve] q=1 -> (q'=q-1);
	[serve_2] q=sp_max -> (q'=q-1);
	[serve_2plus] q>sp_max -> (q'=q-1);
	
endmodule

// apkalpošanas �?trums
const double rate_serve_1 = 4;
const double rate_serve_2 = 2; 
const double rate_serve = rate_serve_1 + rate_serve_2; 

// max darbinieku skaits
const int sp_max = 2;
module SP
// darbinieku skaits
	sp : [0..sp_max] init 0;
// nodarbin�?to skaits pieaug, kad atn�?k klients
	[atnaak] true -> (sp'=min(q+1,sp_max));
// pēdējais ir ticis apkalpots
	[serve] q=1 -> rate_serve_1 : (sp'=0);
// kopējais apkalpošanas �?trums palielin�?s, kad darbiniekam ir klients
	[serve_2] q=sp_max -> rate_serve :(sp'=q-1);
// visi nodarbin?ti, nevar palielin�?t darbinieku skaitu
	[serve_2plus] q>sp_max -> rate_serve : (sp'=sp);

endmodule

// klients apkalpots
rewards "pelna"
	[serve] true : 10;
	[serve_2] true : 10;
	[serve_2plus] true : 10;
endrewards

// klientiem j�?gaida darbinieks, bezmaksas kafija
rewards "kafija"
	[atnaak] q>=sp_max : 2;
endrewards