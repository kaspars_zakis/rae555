dtmc

const double 	p;
const int 	N;

const int L;
module Last
	[] true -> (L'=s);
endmodule
module lietus_N

	s : [0..N] init 0;

	[a] s=0 -> (s'=N);
	
	// N=1 -> 0
	// N=2 -> N
	// [e1] s=1 & N=1 		-> p : (s'=0) 	+ 1-p : (s'=s);
	// [e2] s=1 & N=2 		-> p : (s'=N) 	+ 1-p : (s'=s);
	[e3] s=N-1 & N>2 	-> p : (s'=s-1) + 1-p : (s'=s);

	[e1e2c] s=1 -> p : (s'=N=1?0:N) + 1-p : (s'=N<3?s:s+1);

	// [] s>0 -> p : (s'=N=1?1:N=2?s:);
	
	// N<3 		-> 1
	// 3<=N<5 	-> 10
	// N=5 		-> 5
	// N>5 		-> 10
	// [] s=0 -> (s'=N < 3 ? 1 : (N=5 ? 5 : 10));

	[b] s=N & N>1 		-> p : (s'=1) 	+ 1-p : (s'=0);
	// [c] s=1 & N>2 		-> p : (s'=N) 	+ 1-p : (s'=s+1);
	[d] s>1 & s<N-1 & N>3 	-> p : (s'=s-1) + 1-p : (s'=s+1);

endmodule
 
rewards "saliit"
	s=0 : 1;
endrewards