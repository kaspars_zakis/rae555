dtmc

const double 	p;
const int 	N;

module lietus_N

	s : [0..N] init 0;

	[a] s=0 -> (s'=N);

	[e1] s=1 & N=1 		-> p : (s'=0) 	+ 1-p : (s'=s);
	[e2] s=1 & N=2 		-> p : (s'=N) 	+ 1-p : (s'=s);
	[e3] s=N-1 & N>2 	-> p : (s'=s-1) + 1-p : (s'=s);

	[b] s=N & N>1 		-> p : (s'=1) 	+ 1-p : (s'=0);
	[c] s=1 & N>2 		-> p : (s'=N) 	+ 1-p : (s'=s+1);
	[d] s>1 & s<N-1 & N>3 	-> p : (s'=s-1) + 1-p : (s'=s+1);

endmodule
 
rewards "saliit"
	s=0 : 1;
endrewards