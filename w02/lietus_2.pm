dtmc

const double p;

module lietus

	s : [0..3] init 1;

	[] s=0 -> (s'=3);
	[] s=1 -> p : (s'=3) + 1-p : (s'=2);
	[] s=2 -> p : (s'=1) + 1-p : (s'=2);
	[] s=3 -> p : (s'=1) + 1-p : (s'=0);

endmodule


rewards "cena"
	s=0 : 1;
endrewards



// const int N=3;
// s : [0..N] init 1;
// [a] s=0 -> (s'=N)
// [b] s=N -> p : (s'=1) + 1-p (s'=0);
// [c] s=1 -> p : (s'=s_max) + 1-p : (s'=s-1);
// [d] 1<s<N -> p : (s'=s-1) + 1-p : (s'=s+1);
// [e] s=N-1 -> p : (s'=N-2) + 1-p : (s'=s);
