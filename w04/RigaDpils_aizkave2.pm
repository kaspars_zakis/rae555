dtmc
const int sakums = 1;

module rindas
	s : [0..2] init sakums;
	[Riga] s=0 -> 0.5 : (s'=0) + 0.3 : (s'=1) + 0.2 : (s'=2);
	[Dpils] s=1 -> 0.1 : (s'=0) + 0.8 : (s'=1) + 0.1 : (s'=2);
	[Liep] s=2 -> 0.4 : (s'=0) + 0.5 : (s'=1) + 0.1 : (s'=2);

endmodule

module abc
	n : [0..2] init sakums;
	[Riga] true -> (n'=s);
	[Dpils] true -> (n'=s);
	[Liep] true -> (n'=s);
endmodule
	
rewards "RuzD"
	[Dpils] n=0 : 7;
endrewards
