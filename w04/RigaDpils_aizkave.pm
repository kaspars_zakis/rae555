dtmc
const int sakums = 1;

module rindas
	s : [0..2] init sakums;
	n : [0..2] init sakums;
	[Riga] s=0 -> 0.5 : (s'=0)&(n'=s) + 0.3 : (s'=1)&(n'=s) + 0.2 : (s'=2)&(n'=s);
	[Dpils] s=1 -> 0.1 : (s'=0)&(n'=s) + 0.8 : (s'=1)&(n'=s) + 0.1 : (s'=2)&(n'=s);
	[Liep] s=2 -> 0.4 : (s'=0)&(n'=s) + 0.5 : (s'=1)&(n'=s) + 0.1 : (s'=2)&(n'=s);

endmodule

rewards "RuzD"
	[Dpils] n=0 : 7;
endrewards
