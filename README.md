# RAE555 darbi

Kaspars Zaķis

141REB125

REMC0

[Eksāmena uzdevums](https://bitbucket.org/kaspars_zakis/rae555/wiki/Exam)

RAE 555 kursa ietvaros radītās atskaites un kodu faili
09.2017 - 12.2017

### Week 2
* Salīšanas varbūtības aprēķins ar matricu palīdzību.
	* 30-50-100-iteracijas.fig
	* lietussargs.m
* Mēģinājums modelēt pārvietošanos starp 2 vietām, lietus gadījumā paņemot lietussargu.
	* lietus_2.pm
	* lietus_N.pm
	* lietus_NX.pm
### Week 3
* Rindas modelis.
	* MM12_rindas.pctl
	* MM12_rindas.pm
* Atskaite par padarīto līdz 3. nedēļai.
	* W1_W3.pdf
* Starpeksāmena piemēra mēģinājums.
	* exam.pm
	* exam_f.pm
* Veiksmīgs risinājums salīšanas procesa modelēšanai ar X lietussargiem.
	* lietus_X.pm
### Week 4
* Modelis braukšanai starp 3 pilsētām, kad tikai vienā virzienā ir izmaksas.
	* RigaDpils_aizkave.pm
	* RigaDpils_aizkave2.pm
	* Riga_Dpils
	* Riga_Dpils.gra
	* Riga_Dpils_izmaksas.png
* Salīšanas notikumu skaits 30 dienu laikā atkarībā no salīšanas varbūtības un lietussargu skaita.
	* Salishana0.png
### Week 5
* Frizētavas darbības modelis un darba procesa diagramma.
	* barber_diff.pm
	* barber_flow_diagram.svg
### Week 7
* Visu kuba virsotņu apmeklēšanas modelis, nākamo izvēloties nejauši.
	* cube_full_rand.prism
	* cube_parejas.pdf
* Starpeksāmena uzdevums
	* midterm.pdf
### Week 8
* Visu kuba virsotņu apmeklēšanas modelis, nākamo izvēloties pēc īsākā pārraides laika, ko veido pārraides aizkave un ātrums.
	* cube_end.prism
	* w8_cube.pdf
### Week 11
* Atskaite par paveikto 11. nedēļā - trafika datu apstrāde ar Weka.
	* w11.pdf
### Week 13
* R kods grafiku iegūšanai.
	* R_plotplot.R
* R kods failu lejupielādei no ftp servera
	* tt_1dienuzd.R
* Atskaite par paveikto 13. nedēļā - datu pakešu analīze ar tcpdump.
	* w13.pdf